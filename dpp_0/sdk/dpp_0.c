/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "xparameters.h"
#include "comblock.h"

#define CB_BASE_ADD XPAR_COMBLOCK_0_AXIL_BASEADDR
#define CB_ENA_FIFO 1 // Enable is in bit 0 REG0
#define CB_CLR_FIFO 1 // Reset  is in bit 0 REG1
#define CB_IFIFO_AE 1
#define CB_THRSH    CB_OREG2
#define CB_OP_MOD   CB_OREG3


int main()
{
   int16_t pulse[] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 68, 68, 68, 68, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 350, 350, 350, 350, 350, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 653, 653, 653, 653, 653, 653, 653, 653, 653, 653, 653, 653, 653, 653, 0, 0, 0, 0,
		                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		                0, 0, 0, 0, 0, 0, 0, 0, 264, 264, 264, 264, 264, 264, 264, 264, 264, 264, 264, 264, 264, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 94, 94, 94, 94, 94, 94, 94, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1038, 1038, 1038, 1038, 1038, 1038,
						1038, 1038, 1038, 1038, 1038, 1038, 1038, 1038, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		                0, 0, 0, 0, 0, 0, 0, 28, 28, 28, 28, 28, 28, 28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

   int16_t threshold=80;
   int8_t  op_mode=0; //0: Amplitude measurement, 1: ADC debugging, 2: FIR debugging, 3: derivative FIR output
   int i;
   int status;
   int value;

    printf("DPP first example.\n\r");

    printf("Threshold value written: %d\n\r", threshold);
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_THRSH, threshold);

    printf("Operation mode: %d\n\r", op_mode);
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OP_MOD, op_mode);

    printf("Reset FIFO\n\r");
    cbWrite(CB_BASE_ADD, CB_OREG1, CB_CLR_FIFO);
    cbWrite(CB_BASE_ADD, CB_OREG1, 0);


    status = cbRead(CB_BASE_ADD, CB_IFIFO_STATUS);
    printf("Status Input FIFO before start: words %d, status bits %d \n\r", (status>>16), (status&7));
    status = cbRead(CB_BASE_ADD, CB_OFIFO_STATUS);
    printf("Status Output FIFO before writing: words %d, status bits %d \n\r", (status>>16), (status&7));


   printf("Write values in the FIFO. \n\r");
    for (i = 0; i< 350; i++) {
    	  cbWrite(CB_BASE_ADD, CB_OFIFO_VALUE, pulse[i]);
 //   	cbWrite(CB_BASE_ADD, CB_OREG0, CB_ENA_FIFO);
 //   	cbWrite(CB_BASE_ADD, CB_OREG0, 0);
    }

    status = cbRead(CB_BASE_ADD, CB_OFIFO_STATUS);
    printf("Status Output FIFO after writing: words %d, status bits %d \n\r", (status>>16), (status&7));



    printf("DPP enable section.\n\r");

    status = cbRead(CB_BASE_ADD, CB_IFIFO_STATUS);
    printf("Input FIFO before starting DPP operation: words %d, status bits %d", (status>>16), (status&7));
    printf("\n\r");


    printf("Read data with DPP. \n\r");
    cbWrite(CB_BASE_ADD, CB_OREG0, CB_ENA_FIFO);
    do {
         status = cbRead(CB_BASE_ADD, CB_OFIFO_STATUS);
    } while ((status>>16) != 0);  // Read until there is no more data in output fifo

    cbWrite(CB_BASE_ADD, CB_OREG0, 0);

    status = cbRead(CB_BASE_ADD, CB_IFIFO_STATUS);
    printf("Input FIFO after DPP operation: words %d, status bits %d", (status>>16), (status&7));  // It should be 5
    printf("\n\r");


    status = cbRead(CB_BASE_ADD, CB_OFIFO_STATUS);
    printf("Output FIFO after DPP operation: words %d, status bits %d", (status>>16), (status&7));
    printf("\n\r");

    printf("Read data from DPP FIFO. \n\r");
    status = cbRead(CB_BASE_ADD, CB_IFIFO_STATUS);
    for(i=0; i < (status>>16); i++) {
         value = cbRead(CB_BASE_ADD, CB_IFIFO_VALUE);
         printf("Pulse number: %d has an amplitude of: %d \n\r", i, value);
    };



    printf("Finish test.\n\r");


    return 0;
}
