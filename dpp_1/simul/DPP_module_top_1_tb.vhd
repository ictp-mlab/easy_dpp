----------------------------------------------------------------------------------
-- Company: ICTP MLab
-- Engineer: B. Valinoti -- bvalinot@ictp.it
-- 
-- Create Date: 12/11/2020 01:16:26 PM
-- Design Name: 
-- Module Name: DPP_module_top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library std;
use std.textio.all;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DPP_module_top_tb_1 is
--  Port ( );
end DPP_module_top_tb_1;

architecture Behavioral of DPP_module_top_tb_1 is

   signal clk      : std_logic;
   signal rst      : std_logic;
   signal n_rst    : std_logic;
   signal ena      : std_logic:='0';
   signal cha      : std_logic_vector(15 downto 0):= (others=>'0');
   signal datao    : std_logic_vector(23 downto 0):= (others=>'0');
   signal stop     : boolean := FALSE;
   constant N_SAMPL: integer := 1; 
   type t_int_array is array( integer range <> ) of integer;

   signal data_in        : std_logic_vector (15 downto 0):= (others=>'0');
   signal threshold_high : std_logic_vector (15 downto 0):= (others=>'0');
   signal threshold_low  : std_logic_vector (15 downto 0):= (others=>'0');
   signal op_mode        : std_logic_vector (1 downto 0):= "00";
   signal data_to_fifo   : std_logic_vector (15 downto 0);
   signal cf_1, cf_2, cf_3 : std_logic_vector (4 downto 0):= (others=>'0');

-------				
   signal fifo_wr_en_mux_out :  std_logic;
begin

   do_clock : entity work.Clock
   port map(
      clk_o  => clk,
      rst_o  => rst,
      stop_i => stop
   );
   n_rst <= not(rst);

   dut : entity work.DPP_module_top_1
   port map (
           clk_i  => clk,
           rst_i  => rst,
           ena_i  => ena,
           ADC_data_i => data_in(11 downto 0),
			--Parameters        
           threshold_high_i => threshold_high, -- Absolute threshold to detect pulse arrival
           threshold_low_i  => threshold_low, -- Absolute threshold to go to the "waiting_state" (it should be slightly lower than "threshold_high")
           cf_1_i => cf_1,
           cf_2_i => cf_2,
           cf_3_i => cf_3,
           op_mode_i => op_mode,
           delay_tap_i => (others => '0'),
			--Outputs
           data_o   => data_to_fifo,
           fifo_wr_en_mux_out => fifo_wr_en_mux_out
      );


   test_dut : process
      file cha_file   : text open read_mode is "ecal_pulse.csv";
      variable a_row  : line;
      variable a_read : t_int_array(1 to N_SAMPL);
      begin
         wait for 200 ns;
         cf_1 <= "00001";  --1
         cf_2 <= "00011";  --3
         cf_3 <= "00101";  --5
         wait for 10 ns;
         ena <= '1';
         threshold_high <= std_logic_vector(to_unsigned(750,16));
         threshold_low  <= std_logic_vector(to_unsigned(650,16));
         file_open(cha_file, "salida.csv", READ_MODE);
         
         -- signal read from FILE
         while not endfile(cha_file)  loop
            readline(cha_file, a_row);
            for i in 1 to N_SAMPL loop
               read(a_row,a_read(i));
               cha <= std_logic_vector(to_unsigned(a_read(i),16));
               wait for 1 ns;
               data_in <= cha;
               report "The value of adcA_" & integer'image(i) &" is: " & integer'image(a_read(i));
               wait until rising_edge(clk);
            end loop;
         end loop;
         wait for 200 ns;
         stop <= TRUE;
         wait for 1 ns;
         file_close(cha_file);
         wait;
   end process test_dut;

--- Here write the assertion part


end Behavioral;
